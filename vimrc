let sbv_indentation_length=2
" Enabled / Disabled placeholder chars
" ijeif
let sbv_display_placeholder=1
" Charactere placeholder for tabulation [2 char]
let sbv_tab_placeholder='»·'
" Charactere placeholder for space [1 char]
let sbv_space_placeholder='·'

execute pathogen#infect()
syntax on
set number
filetype plugin on
set clipboard=unnamed
call pathogen#helptags()
let g:rehash256 = 1
set t_Co=256
colorscheme molokai
" autocmd VimEnter * NERDTree
autocmd BufWinEnter,WinEnter * setlocal colorcolumn=80
autocmd BufWinLeave,WinLeave * setlocal colorcolumn=0
noremap <C-t> :tabedit 
noremap <C-l> <C-w><Right>
noremap <C-j> <C-w><Up>
noremap <C-k> <C-w><Down>
noremap <C-h> <C-w><Left>
noremap <C-r> :NERDTree<CR>

noremap <S-Tab>				:tabprevious<CR>
noremap <Tab>	:tabnext<CR>

noremap <C-x>	:shell<CR>

execute "set tabstop=". sbv_indentation_length
execute "set shiftwidth=". sbv_indentation_length
execute "set softtabstop=". sbv_indentation_length
set expandtab


if !empty(sbv_display_placeholder)
	execute "set list listchars=tab:". sbv_tab_placeholder .",trail:". sbv_space_placeholder
endif

map <S-v> :r !xclip -o<CR>
